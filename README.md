# Каталог изображений

Web-приложение для хранения и управления изображения

# Как развернуть

```bash
$ cp .env.example .env
$ docker-compose up -d
$ pip install -r requirements.txt
$ python src/manage.py makemigrations
$ python src/manage.py migrate --settings=core.settings.local
$ yarn && yarn run serve
$ python src/manage.py runserver --settings=core.settings.local
```

# Demo доступ
admin
?8L~8*M9UThDVX3E
