(function ($) {
  $(document).ready(function () {
    $("#id_date_taken").datepicker({
      autoHide: true,
      format: 'dd.mm.yyyy',
      container: true,
    });
  });
})(jQuery);
