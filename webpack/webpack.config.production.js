'use strict';
const webpack = require('webpack');
const BundleTracker = require('webpack-bundle-tracker');
const config = require('./webpack.config.base.js');
const ExtractTextPlugin = require("extract-text-webpack-plugin");

config.output.path = require('path').resolve(__dirname, '../src/static/bundles/prod/');

config.plugins = config.plugins.concat([
  new BundleTracker({filename: './webpack-stats-production.json'}),
  new webpack.DefinePlugin({
    'process.env': {
      'NODE_ENV': JSON.stringify('production'),
    }
  }),
  new webpack.optimize.OccurrenceOrderPlugin(),
  new webpack.optimize.UglifyJsPlugin({
    compressor: {
      warnings: false
    }
  }),
  new ExtractTextPlugin({filename: 'styles/[name]-[hash].css', disable: false, allChunks: true})
]);

config.module.loaders.push(
  {
    test: /\.jsx?$/,
    exclude: /(node_modules|bower_components)/,
    use: ['babel-loader']
  },
  {
    test: /\.(sass|scss)$/,
    exclude: /(node_modules|bower_components)/,
    use: ExtractTextPlugin.extract({
      fallback: 'style-loader',
      use: ['css-loader', 'sass-loader']
    })
  },
  {
    test: /\.css$/,
    exclude: /(node_modules|bower_components)/,
    use: ExtractTextPlugin.extract({
      fallback: 'style-loader',
      use: [{
        loader: 'css-loader',
      }]
    })
  }
);

module.exports = config;
