'use strict';
const path = require("path");
const webpack = require('webpack');
const BundleTracker = require('webpack-bundle-tracker');
const config = require('./webpack.config.base.js');

/* Локальный IP */
const IP = '127.0.0.1';
const PORT = '3000';

config.devtool = '#eval-source-map';
// config.ip = IP;

config.entry = [
  'babel-polyfill',
  'webpack-dev-server/client?http://' + IP + ':' + PORT,
  'webpack/hot/dev-server',
  './js/index.js',
];

// override django's STATIC_URL for webpack bundles
config.output.publicPath = 'http://' + IP + ':' + PORT + '/static/bundles/';

// Add HotModuleReplacementPlugin and BundleTracker plugins
config.plugins = config.plugins.concat([
  new webpack.HotModuleReplacementPlugin(),
  new webpack.NoEmitOnErrorsPlugin(),
  new BundleTracker({filename: './webpack-stats-local.json'}),
  new webpack.SourceMapDevToolPlugin({filename: '[file].map'}),
]);

// Add a loader for JSX files with react-hot enabled
config.module.loaders.push(
  {
    test: /\.jsx?$/,
    exclude: /(node_modules|bower_components)/,
    use: ['babel-loader']
  },
  {
    test: /\.(sass|scss)$/,
    exclude: /(node_modules|bower_components)/,
    use: [
      'style-loader', 'css-loader', 'sass-loader'
    ]
  },
  {
    test: /\.css$/,
    exclude: /(node_modules|bower_components)/,
    use: [
      'style-loader', 'css-loader?sourceMap'
    ]
  }
);

module.exports = config;
