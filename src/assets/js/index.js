'use strict';

import $ from 'jquery'
import '../scss/main.scss'

$(document).ready(() => {


  function getCookie(name) {
    let cookieValue = null;
    let i = 0;
    if (document.cookie && document.cookie !== '') {
      let cookies = document.cookie.split(';');
      for (i; i < cookies.length; i++) {
        let cookie = jQuery.trim(cookies[i]);
        // Does this cookie title begin with the name we want?
        if (cookie.substring(0, name.length + 1) === (name + '=')) {
          cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
          break;
        }
      }
    }
    return cookieValue;
  }

  let csrftoken = getCookie('csrftoken');

  function csrfSafeMethod(method) {
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
  }

  $.ajaxSetup({
    crossDomain: false,
    beforeSend: function (xhr, settings) {
      if (!csrfSafeMethod(settings.type)) {
        xhr.setRequestHeader("X-CSRFToken", csrftoken);
      }
    }
  });


  let onDelete = function () {
    $.post(this.href, function (data) {
      if (data.result == "ok") {
        location.reload();
      }
    }).fail(function (err) {
      alert(err);
    });
    return false;
  }

  $(".delete").click(onDelete);


});
