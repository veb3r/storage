from django.db import models
from datetime import datetime
from ckeditor.fields import RichTextField


class Image(models.Model):
    """
    Image model
    """
    title = models.CharField(verbose_name='Название', max_length=75, default='')
    image = models.ImageField(verbose_name='Изображение', default='')
    description = RichTextField(verbose_name='Описание', default='')
    date_taken = models.DateField(verbose_name='Дата сьемки', default=datetime.now(), db_index=True)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ('date_taken',)
        verbose_name = 'Изображение'
        verbose_name_plural = 'Изображения'
