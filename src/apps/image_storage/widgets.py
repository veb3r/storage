from django.forms import widgets


class Datepicker(widgets.DateInput):
    class Media:
        css = {
            'all': ('datepicker/datepicker.min.css',)
        }
        js = (
            'datepicker/jquery.min.js',
            'datepicker/datepicker.min.js',
            'datepicker/field.js',
        )
