from django.contrib import admin
from django.utils.safestring import mark_safe
from .models import Image


@admin.register(Image)
class ImageAdmin(admin.ModelAdmin):
    save_as_continue = True
    save_on_top = True
    list_display = (
        'title',
        'date_taken',
        'get_rich_description',
        'get_image',
    )
    date_hierarchy = 'date_taken'

    def get_image(self, obj):
        return mark_safe('<img width="150px" height="auto" src={}>'.format(obj.image.url))

    get_image.short_description = 'Изображение'

    def get_rich_description(self, obj):
        return mark_safe(obj.description)

    get_rich_description.short_description = 'Описание'
