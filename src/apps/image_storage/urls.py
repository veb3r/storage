from django.conf.urls import url
from .views import ListImage, UpdateView, AddImage, DeleteImage, search

urlpatterns = [
    url(r'^$', ListImage.as_view(), name='index'),
    url(r'image-(?P<pk>\d+)/edit/', UpdateView.as_view(), name='edit'),
    url(r'add/', AddImage.as_view(), name='add'),
    url(r'image-(?P<pk>\d+)/delete/', DeleteImage.as_view(), name='delete'),

    url(r'search/', search, name='search'),
]
