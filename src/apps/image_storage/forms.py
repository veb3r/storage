from ckeditor.widgets import CKEditorWidget
from django import forms
from .models import Image
from .widgets import Datepicker


class ImageForm(object):
    bootstrap_form_class = 'form-control'
    description = forms.CharField(widget=CKEditorWidget(), label='Описание')

    def __init__(self, *args, **kwargs):
        super(ImageForm, self).__init__(*args, **kwargs)
        self.fields['date_taken'].label = 'Укажите дату сьемки'
        for name, field in self.fields.items():
            if 'class' in field.widget.attrs:
                field.widget.attrs['class'] += ' {}'.format(self.bootstrap_form_class)
            else:
                field.widget.attrs.update({'class': self.bootstrap_form_class})


class ImageEditForm(ImageForm, forms.ModelForm):
    date_taken = forms.DateField(widget=Datepicker(), label='Укажите дату сьемки')

    class Meta:
        model = Image
        fields = (
            'title',
            'description',
            'date_taken',
        )


class ImageUploadForm(ImageForm, forms.ModelForm):
    date_taken = forms.DateField(widget=Datepicker(), label='Укажите дату сьемки')

    class Meta:
        model = Image
        fields = (
            'title',
            'image',
            'description',
            'date_taken',
        )
