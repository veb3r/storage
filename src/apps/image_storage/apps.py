from django.apps import AppConfig


class ImageStorageConfig(AppConfig):
    name = 'apps.image_storage'
    verbose_name = 'Хранитель картинок'
