from django.contrib import messages
from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views import generic

from .filter import ImageFilter
from .models import Image
from .forms import ImageUploadForm, ImageEditForm


def search(request):
    template_name = 'image_storage/search.html'

    _filter = ImageFilter(request.GET, queryset=Image.objects.all())
    return render(request, template_name, {
        'filter': _filter
    })


class ListImage(generic.ListView):
    model = Image
    template_name = 'image_storage/index.html'
    context_object_name = 'images'
    paginate_by = 9


class UpdateView(generic.UpdateView):
    model = Image
    form_class = ImageEditForm
    template_name = 'image_storage/_update_image.html'
    context_object_name = 'image'
    success_message = 'Изображение изменено'

    def form_valid(self, form):
        super(UpdateView, self).form_valid(form)
        messages.success(self.request, self.success_message)
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse_lazy('storage:edit', kwargs={'pk': self.get_object().id})


class AddImage(generic.FormView):
    form_class = ImageUploadForm
    template_name = 'image_storage/_add_image.html'
    success_message = 'Изображение добавлено'

    def form_valid(self, form):
        form.save()
        messages.success(self.request, self.success_message)
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse_lazy('storage:add')


class DeleteImage(generic.DeleteView):
    model = Image
    success_url = reverse_lazy('storage:index')

    def dispatch(self, *args, **kwargs):
        response = super(DeleteImage, self).dispatch(*args, **kwargs)
        if self.request.is_ajax():
            response_data = {"result": "ok"}
            return JsonResponse(response_data)
        else:
            return response
