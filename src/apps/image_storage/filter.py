import django_filters
from django import forms

from .models import Image


class ImageFilter(django_filters.FilterSet):
    title = django_filters.CharFilter(lookup_expr='icontains', label='Искать по названию',
                                      widget=forms.TextInput(attrs={'class': 'form-control'}))
    description = django_filters.CharFilter(lookup_expr='icontains', label='Искать по описанию',
                                            widget=forms.TextInput(attrs={'class': 'form-control'}))

    class Meta:
        model = Image
        fields = (
            'title',
            'description',
            'date_taken',
        )


