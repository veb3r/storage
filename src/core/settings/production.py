from .base import *
from decouple import config

DEBUG = False

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': config('POSTGRES_DB'),
        'USER': config('POSTGRES_USER'),
        'PASSWORD': config('POSTGRES_PASSWORD'),
        'HOST': config('DB_HOST'),
        'PORT': config('DB_PORT'),
    }
}

STATICFILES_DIRS = (os.path.join(SITE_ROOT, 'static'),)

if not DEBUG:
    WEBPACK_LOADER.update({
        'DEFAULT': {
            'BUNDLE_DIR_NAME': 'bundles/prod/',
            'STATS_FILE': os.path.join(SITE_ROOT, 'webpack-stats-production.json')
        }
    })
